import {POST} from "@/utils/api";

let generOpt = null

export async function findCustomOpt(initCode) {
  if (initCode === '0000' && generOpt !== null) {
    return generOpt
  }
  let opt
  var params = new URLSearchParams()
  params.append('customsCode', initCode)
  await POST('/baseParam/getCustomsMap', params).then(response => {
    let {data} = response.data
    if (data.length === 0) {
      return
    }
    let option = []
    // 第一级
    let firstLevel = data.some(item => {
      return item.code === '0000'
    })
    // 第二级
    let secondLevel = data.filter(element => /00$/.test(element.code)).filter(element => element.code !== '0000')
    let newSecondLevel = []
    if (secondLevel.length > 0) {
      secondLevel.map(item => {
        let flag = true
        let header = item.id.substr(0, 2)
        data.forEach(each => {
          if (new RegExp('^' + header + '\\d+$').test(each.code)
            && each.code.substr(each.code.length - 2) !== '00') {
            if (flag) {
              item.children = []
              flag = false
            }
            item.children.push({
              id: each.code,
              lable: each.name
            })
          }
        })
      })
      if (firstLevel) {
        option.push({
          id: data[0].code,
          lable: data[0].name,
          children: [...newSecondLevel]
        })
      } else {
        option = [...newSecondLevel]
      }
    } else {
      data.map(item => {
        option.push({
          id: item.code,
          lable: item.name
        })
      })
    }
    opt = option
    if (initCode === '0000') {
      generOpt = opt
    }
  })
  return opt
}
