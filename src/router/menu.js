const {SERVICEID} = require('@/config.js')
// 菜单定义
const
  menu = [
    {path: '/${SERVICEID}/home/index', meta: {title: '首页', icon: 'fa fa-home'}}
  ]
export default menu
