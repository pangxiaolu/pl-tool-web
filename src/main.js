import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VueRouter from "vue-router"
import App from './App'

Vue.use(ElementUI, {size: 'small', zIndex: 3000})
Vue.use(VueRouter)
/* eslint-disable no-new */
const router = new VueRouter(
  {
    mode: 'hash', Routers
  }
)
let vue = new Vue({
  el: '#app',
  VueRouter,
  components: {App},
  template: '<App/>'
})

Vue.use({vue})
