// 字符显示格式转换
export const Formatter = {}

Formatter.time = function (value) {
  if (value === undefined || value === null) {
    return ''
  }
  if (value.indexof('/') > -1) {
    value = value.replace('/Date(', '').replace(')/', '').split('+')[0]
  }
  let da = new Date(value)
  return (
    da.getFullYear()
    + '-' + (da.getMonth() + 1 < 10 ? '0' + (da.getMonth() + 1) : da.getMonth() + 1)
    + '-' + (da.getDate() < 10 ? '0' + da.getDate() : da.getDate())
    + ' ' + (da.getHours() < 10 ? '0' + da.getHours() : da.getHours())
    + ':' + (da.getMinutes() < 10 ? '0' + da.getMinutes() : da.getMinutes())
    + ':' + (da.getSeconds() < 10 ? '0' + da.getSeconds() : da.getSeconds())
  )
}

Formatter.transfor = function (row, column,opt){
  var value =''
  opt.forEach(item=>{
    if (item.value === row[column.property]) {
      value = item.label
    }
    return value
  })
}

export default Formatter
