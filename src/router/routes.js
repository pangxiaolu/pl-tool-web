const {SERVICEID} = require('@/config.js')
// 路由定义
const Routes = [
  {name: 'home', path: '/', component: '/home/index'},
  {name: 'home', path: '/${SERVICEID}/', component: '/home/index'}
]
export default Routes
