import Request from "@/utils/request";

export function POST(url, params) {
  return Request({
    url: '/api/pl-tool-service/${url}?${params}',
    method: 'post',
    data: params
  })
}

export function GET(url, params) {
  return Request({
    url: '/api/pl-tool-service/${url}?${params}',
    method: 'get',
    data: params
  })
}

export function PATCH(url, params) {
  return Request({
    url: '/api/pl-tool-service/${url}?${params}',
    method: 'patch',
    data: params
  })
}

export function DELETE(url, params) {
  return Request({
    url: '/api/pl-tool-service/${url}?${params}',
    method: 'delete',
    data: params
  })
}

export function CHECKURL(url, params) {
  return Request({
    url: '/api/pl-tool-service/${url}',
    method: 'get',
    data: params
  })
}

export function POSTBODY(url, params) {
  return Request({
    url: '/api/pl-tool-service/${url}',
    method: 'post',
    data: params
  })
}

export function POSTFILE(url, params) {
  return Request({
    url: '/api/pl-tool-service/${url}',
    method: 'post',
    data: params,
    reposneType: 'arraybuffer'
  })
}
